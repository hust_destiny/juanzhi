//
//  main.m
//  juanzhi
//
//  Created by 仇呈燕 on 16/1/21.
//  Copyright © 2016年 zhouxin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
