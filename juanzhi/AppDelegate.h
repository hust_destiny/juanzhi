//
//  AppDelegate.h
//  juanzhi
//
//  Created by 仇呈燕 on 16/1/21.
//  Copyright © 2016年 zhouxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

